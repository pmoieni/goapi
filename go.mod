module gitlab.com/pmoieni/goapi

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/joho/godotenv v1.4.0
	github.com/lestrrat-go/jwx v1.2.23
	github.com/mikespook/gorbac v1.0.1 // indirect
	github.com/mikespook/gorbac/v2 v2.3.3
	github.com/rs/cors v1.8.2
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.21.0
	gorm.io/driver/mysql v1.3.3
	gorm.io/gorm v1.23.4
)
