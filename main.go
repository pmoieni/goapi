 package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/pmoieni/goapi/api"
	"gitlab.com/pmoieni/goapi/api/config"
	"gitlab.com/pmoieni/goapi/api/utils"
	"gitlab.com/pmoieni/goapi/api/db"
	"go.uber.org/zap"
)

func main() {
	// initialize logger
	utils.InitLogger()

	// initialize config
	err := godotenv.Load()
	if err != nil {
		utils.Logger.Fatal("Error getting env", zap.Error(err))
	} else {
		utils.Logger.Info("getting the env values")
	}

	config := config.NewConfig()

	// initialize database
	db.InitializeDB(config.DB_DRIVER, config.DB_USER, config.DB_PASSWORD, config.DB_PORT, config.DB_HOST, config.DB_NAME)

	server := api.Server{}

	server.Init()
	server.Run("8080")
}
