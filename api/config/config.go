package config

import (
	"os"
)

// Config defines the shape of the configuration values used across the api
type Config struct {
	ENV                       string
	API_PORT                  string
	DB_DRIVER                 string
	DB_HOST                   string
	DB_USER                   string
	DB_NAME                   string
	DB_PASSWORD               string
	DB_PORT                   string
	ACCESS_TOKEN_HMAC_SECRET  []byte
	REFRESH_TOKEN_HMAC_SECRET []byte
	ID_TOKEN_HMAC_SECRET      []byte
}

// NewConfig returns the default configuration values used across the api
func NewConfig() *Config {
	// set defaults - these can be overwritten via command line

	if os.Getenv("GO_ENV") == "" {
		os.Setenv("GO_ENV", "development")
	}

	if os.Getenv("JWT_ISSUER") == "" {
		os.Setenv("JWT_ISSUER", "http://localhost:8080")
	}

	if os.Getenv("API_PORT") == "" {
		os.Setenv("API_PORT", "8080")
	}

	if os.Getenv("DB_DRIVER") == "" {
		os.Setenv("DB_DRIVER", "mysql")
	}

	if os.Getenv("DB_HOST") == "" {
		os.Setenv("DB_HOST", "127.0.0.1")
	}

	if os.Getenv("DB_USER") == "" {
		os.Setenv("DB_USER", "pmoieni")
	}

	if os.Getenv("DB_PASSWORD") == "" {
		os.Setenv("DB_PASSWORD", "p.1306.6031")
	}

	if os.Getenv("DB_NAME") == "" {
		os.Setenv("DB_NAME", "square_music")
	}

	if os.Getenv("DB_PORT") == "" {
		os.Setenv("DB_PORT", "3306")
	}

	return &Config{
		ENV:                       os.Getenv("GO_ENV"),
		API_PORT:                  os.Getenv("PORT"),
		DB_DRIVER:                 os.Getenv("DB_DRIVER"),
		DB_HOST:                   os.Getenv("DB_HOST"),
		DB_USER:                   os.Getenv("DB_USER"),
		DB_NAME:                   os.Getenv("DB_NAME"),
		DB_PASSWORD:               os.Getenv("DB_PASSWORD"),
		DB_PORT:                   os.Getenv("DB_PORT"),
		ACCESS_TOKEN_HMAC_SECRET:  []byte(os.Getenv("ACCESS_TOKEN_HMAC_SECRET")),
		REFRESH_TOKEN_HMAC_SECRET: []byte(os.Getenv("REFRESH_TOKEN_HMAC_SECRET")),
		ID_TOKEN_HMAC_SECRET:      []byte(os.Getenv("ID_TOKEN_HMAC_SECRET")),
	}
}
