package api

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/pmoieni/goapi/api/utils"
	"github.com/rs/cors"
)

type Server struct {
	Router  *chi.Mux
	Handler http.Handler
}

func (s *Server) Run(addr string) {
	// The HTTP Server
	server := &http.Server{
		Addr:         ":" + addr,        // configure the bind address
		Handler:      s.Router,          // set the default handler
		ErrorLog:     utils.StdLogger,   // set the logger for the server
		ReadTimeout:  10 * time.Second,  // max time to read request from the client
		WriteTimeout: 10 * time.Second,  // max time to write response to the client
		IdleTimeout:  120 * time.Second, // max time for connections using TCP Keep-Alive
	}

	// Server run context
	serverCtx, serverStopCtx := context.WithCancel(context.Background())

	// Listen for syscall signals for process to interrupt/quit
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sig

		// Shutdown signal with grace period of 30 seconds
		shutdownCtx, cancel := context.WithTimeout(serverCtx, 30*time.Second)

		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				utils.Logger.Fatal("graceful shutdown timed out.. forcing exit.")
			}
		}()

		// Trigger graceful shutdown
		err := server.Shutdown(shutdownCtx)
		if err != nil {
			utils.Logger.Fatal(err.Error())
		}
		serverStopCtx()

		defer cancel()
	}()

	// Run the server
	err := server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		utils.Logger.Fatal(err.Error())
	}

	// Wait for server context to be stopped
	<-serverCtx.Done()
}

func (s *Server) Init() {
	s.Router = chi.NewRouter()

	s.Router.Use(middleware.RequestID)
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000"}, // in production should contain origins of clients
		AllowCredentials: true,
		AllowedHeaders:   []string{"Content-Type", "Authorization", "ID-Token"},
		Debug:            true, // only for test. disable in production
	})
	s.Router.Use(c.Handler)

	s.InitRoutes()
}
