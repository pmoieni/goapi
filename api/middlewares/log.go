package middlewares

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/pmoieni/goapi/api/models"
	"gitlab.com/pmoieni/goapi/api/utils"
	"go.uber.org/zap"
)

func Log(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now().UTC()

		headersJSON, err := json.Marshal(r.Header)
		if err != nil {
			utils.Logger.Error("couldn't marshal request header", zap.Error(err))
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		bodyBuf, err := ioutil.ReadAll(r.Body)
		if err != nil {
			utils.Logger.Error("couldn't read request body buffer", zap.Error(err))
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		reader := ioutil.NopCloser(bytes.NewBuffer(bodyBuf))
		r.Body = reader

		reqLogJSON, err := json.Marshal(
			&models.RequestLog{
				Time:     time.Now().UTC(),
				Duration: time.Now().UTC().Sub(startTime),
				Method:   r.Method,
				URL:      r.URL.String(),
				Body:     bodyBuf,
				Headers:  headersJSON,
			},
		)
		if err != nil {
			utils.Logger.Error("couldn't marshal request log object: ", zap.Error(err))
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		reqRawJSON := json.RawMessage(reqLogJSON)

		utils.Logger.Info("request log", zap.Any("info", &reqRawJSON))
		next.ServeHTTP(w, r)
	})
}
