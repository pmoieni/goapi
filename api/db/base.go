package db

import (
	"errors"
	"fmt"

	"gitlab.com/pmoieni/goapi/api/models"
	"gitlab.com/pmoieni/goapi/api/utils"
	"go.uber.org/zap"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	gormLogger "gorm.io/gorm/logger"
)

var GlobalDB *gorm.DB

var ErrDBConnection = errors.New("couldn't connect to database")

func InitializeDB(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) {
	var err error

	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
	GlobalDB, err = gorm.Open(mysql.Open(DBURL), &gorm.Config{
		Logger: gormLogger.Default.LogMode(gormLogger.Info),
	})

	if err != nil {
		utils.Logger.Fatal(ErrDBConnection.Error(), zap.Error(err))
	} else {
		utils.Logger.Info("connected to the database")
	}

	//database migration (add "," between models to add more)
	GlobalDB.AutoMigrate(&models.User{}, &models.RefreshToken{})
}
