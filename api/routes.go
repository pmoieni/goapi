package api

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/pmoieni/goapi/api/handlers"
	"gitlab.com/pmoieni/goapi/api/middlewares"
)

// route: "/"
func (s *Server) InitRoutes() {
	s.Router.Mount("/api", initVervions())
}

// route: "/api"
func initVervions() chi.Router {
	r := chi.NewRouter()
	r.Mount("/v1", initV1())

	return r
}

// route: "/api/v1"
func initV1() chi.Router {
	r := chi.NewRouter()
	r.Mount("/auth", initAuthRoutes())
	r.Mount("/account", initAccountRoutes())
	r.Mount("/users", initUserRoutes())

	return r
}

// route: "/api/v1/auth"
func initAuthRoutes() chi.Router {
	r := chi.NewRouter()
	r.Post("/register", handlers.Register)
	r.Post("/login", handlers.Login)
	r.Get("/token", handlers.RefreshToken)

	return r
}

func initAccountRoutes() chi.Router {
	r := chi.NewRouter()
	r.Route("/", func(r chi.Router) {
		r.Use(middlewares.IsAuthenticated)
		r.Get("/", handlers.GetUserAccount)
	})

	return r
}

func initUserRoutes() chi.Router {
	r := chi.NewRouter()
	r.Route("/", func(r chi.Router) {
		r.Use(middlewares.IsAuthenticated)
		r.Use(middlewares.IsAdmin)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("you'll get this if you're logged in"))
		})
		r.Post("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("gotcha"))
		})
	})

	return r
}
