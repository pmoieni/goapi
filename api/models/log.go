package models

import (
	"encoding/json"
	"time"
)

type RequestLog struct {
	Time     time.Time       `json:"time"`
	Duration time.Duration   `json:"duration"`
	Method   string          `json:"method"`
	URL      string          `json:"url"`
	Body     json.RawMessage `json:"body"`
	Headers  json.RawMessage `json:"headers"`
}

type ResponseLog struct {
	Code    int64           `json:"code"`
	Body    json.RawMessage `json:"body"`
	Headers json.RawMessage `json:"headers"`
}
