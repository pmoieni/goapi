package models

type ErrorResponse struct {
	Status  int
	Message string
}

func (er *ErrorResponse) Error() string {
	return er.Message
}
